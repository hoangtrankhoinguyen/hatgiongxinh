@extends('layout.admin.masterLayout')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hien Thi</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tên</label>
                                    <p class="form-control-static">{{$product->name}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <p class="form-control-static">{{$product->price}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Don vi (Unit)</label>
                                    <p class="form-control-static">{{$product->unit}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Anh</label>
                                    <img src="{{asset('uploads/'.$product->image)}}" width="300", height="300"/>
                                </div>
                                <div class="form-group">
                                    <label>Mo ta ngan</label>
                                    <p class="form-control-static">{{$product->short_description}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Mo ta</label>
                                    <p class="form-control-static">{!!$product->long_description!!}</p>
                                </div>
                                <div class="form-group">
                                    <label>Danh Muc</label>
                                    <p class="form-control-static">{{$product->Category->name}}</p>
                                </div>
                                <a href="{{route('products.edit', $product->id)}}" class="btn btn-default">Edit</a>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()