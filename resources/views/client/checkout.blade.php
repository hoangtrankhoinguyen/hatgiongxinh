@extends('layout.client.masterLayout')

@section('content')
<!-- page wapper-->
<div class="columns-container">
	<div class="container" id="columns">
		<!-- page heading-->
		<h2 class="page-heading">
			<span class="page-heading-title">THANH TOÁN ĐƠN HÀNG</span>
		</h2>
		<!-- ../page heading-->
		<div class="page-content page-order">
			<div class="panel-group checkout-page accordion scrollable" id="checkout-page">


				<!-- BEGIN PAYMENT ADDRESS -->
				<div id="payment-address" class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">
							<a data-toggle="collapse" data-parent="#checkout-page" href="#payment-address-content" class="accordion-toggle">
								Điền thông tin để chúng tôi chuyển hàng cho bạn
							</a>
						</h2>
					</div>
					<div id="payment-address-content" class="panel-collapse collapse in">
						<div class="panel-body row">
							{{ Form::open(['route' => 'doCheckout','method' => 'POST','files' => true]) }}
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="firstname">Tên quý khách: <span class="require">*</span></label>
									<input type="text" name="client_name" id="firstname" class="input form-control">
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<label for="lastname">Số điện thoại: <span class="require">*</span></label>
									<input type="text" name="client_phone" id="lastname" class="input form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="address1">Địa chỉ nhận hàng</label>
									<textarea type="text" name="client_address" id="address1" class="input form-control"></textarea>
								</div>
							</div>
							<hr>
							<div class="col-md-12">
								<button class="button  pull-left" type="submit" data-toggle="collapse" data-parent="#checkout-page" data-target="#payment-method-content" id="button-payment-address">Gửi đơn hàng</button>
							</div>
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- ./page wapper-->
@endsection