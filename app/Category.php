<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $fillable = ['name','description','status', 'parent_id','type', 'order','isIndex'];

    public function Product(){
    	return $this->hasMany('App\Product');
    }

    public function New(){
    	return $this->hasMany('App\New');
    }

    public function Childs(){
    	return $this->hasMany('App\Category', 'parent_id');
    }

}
