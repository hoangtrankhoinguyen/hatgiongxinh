@extends('layout.client.masterLayout')
@section('content')
<div class="columns-container">
    <div class="container" id="columns">
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block"><span class="fa fa-list"></span> CATEGORIES</p>

                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    @foreach($sidebar as $item)
                                    <li>
                                        <span></span><a href="{{route('chi-tiet', $item->id)}}">{{$item->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- block video -->
                <div class="block left-module">
                    <p class="title_block"><span class="fa fa-video-camera"></span> VIDEOS</p>

                    <div class="block_content">
                        <iframe width="255" height="300" src="https://www.youtube.com/embed/oFuC0URYKqU" frameborder="0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <!-- ./block video  -->

            </div>

            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title"><span class="fa fa-newspaper-o"></span> NEWS</span>
                </h2>
                <!-- ../page heading-->
                <ul class="blog-posts">
                	@foreach($tintucs as $tintuc)
                    <li class="post-item">
                        <article class="entry">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="entry-ci">
                                        <h3 class="entry-title"><a href="{{route('chi-tiet', $tintuc->id)}}">{{$tintuc->title}} </a></h3>

                                        <div class="entry-meta-data">
                                            <span class="author">
                                        <i class="fa fa-user"></i>
                                            <span class="date"><i class="fa fa-calendar"></i> {{$tintuc->created_at}} </span>
                                        </div>
                                        <div class="entry-excerpt">
                                            {{ str_limit($tintuc->title, 25) }}
                                        </div>
                                        <div class="entry-more">
                                            <a href="{{route('chi-tiet', $tintuc->id)}}">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </li>
                    @endforeach
                </ul>
                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            {{ $tintucs->links() }}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
@endsection