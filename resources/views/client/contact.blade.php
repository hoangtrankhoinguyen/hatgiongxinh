@extends('layout.client.masterLayout')

@section('content')
<div class="page-top page-contact-us">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2 class="page-heading">
					<span class="page-heading-title">LIÊN HỆ</span>
				</h2>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d958.485586650931!2d108.14583372918332!3d16.068481299305105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142192a5a7e73ad%3A0xa372cae3c1544880!2zU2hvcCBI4bqhdCBHaeG7kW5nIFhpbmggxJDDoCBO4bq1bmc!5e0!3m2!1svi!2s!4v1525340206930"
					width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="col-md-9 col-sm-9">
					<h2>Quý khách vui lòng điền thông tin liên hệ. Chúng tôi sẽ phản hồi ngay sau khi nhận được</h2>
					<!-- BEGIN FORM-->
					{{ Form::open(['route' => 'post-lien-he','method' => 'POST']) }}
						<div class="form-group">
							<label for="contacts-name">Tên</label>
							<input type="text" class="input form-control" id="contacts-name" name="name">
						</div>
						<div class="form-group">
							<label for="contacts-email">Email</label>
							<input type="email" class="input form-control" id="contacts-email" name="email">
						</div>
						<div class="form-group">
							<label for="contacts-email">Phone</label>
							<input type="text" class="input form-control" id="contacts-email" name="phone">
						</div>
						
						<div class="form-group">
							<label for="contacts-message">Nội dung tin nhắn</label>
							<textarea class="input form-control" rows="5" id="contacts-message" name="content"></textarea>
						</div>
						<button class="button" type="submit">GỬI NGAY<i class=" fa fa-send"></i></button>
						<button type="button" id="btn-remove-all-item" class="button-cancel margin-right-20">HỦY</button>
					{{ Form::close() }}
					<!-- END FORM-->
				</div>

				<div class="col-md-3 col-sm-3 sidebar2">
					<h2>THÔNG TIN</h2>
					<address>
						<strong>Shop Hạt Giống Xinh</strong><br>
						- 160 Đoàn Phú Tứ, Liên Chiểu, Đà Nẵng<br>
						<abbr title="Phone">- Số Điện Thoại:</abbr> 0975.310.717
					</address>
					<address>
						<strong>Email</strong><br>
						<a href="mailto:info@email.com">trandiep56@gmail.com</a><br>
					</address>
					<ul class="social-icons margin-bottom-40">
						<li>
							<a href="#" data-original-title="facebook" class="facebook"></a>
						</li>
						<li>
							<a href="#" data-original-title="github" class="github"></a>
						</li>
						<li>
							<a href="#" data-original-title="Goole Plus" class="googleplus"></a>
						</li>
						<li>
							<a href="#" data-original-title="linkedin" class="linkedin"></a>
						</li>
						<li>
							<a href="#" data-original-title="rss" class="rss"></a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection