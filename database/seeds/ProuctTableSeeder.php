<?php

use Illuminate\Database\Seeder;
use App\Category;
use Faker\Factory as Faker;

class ProuctTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
        $categories = Category::where('type', 1)->pluck('id')->all();
        foreach (range(1, 100) as $index) {
            $name = $faker->sentence($nbWords = 2, $variableNbWords = true);
            \App\Product::create([
				'name' => $name,
                'slug' => str_slug($name),
				'price' => $faker->randomNumber(5),
				'short_description' => $faker->text($maxNbChars = 200),            
				'long_description' => $faker->randomHtml(2,3),
				'image' =>rand(1, 3).'.jpg',
				'unit' => $faker->word,
				'category_id' => $faker->randomElement($categories),      
                'created_at' => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now')
            ]);
        }

    }
}
