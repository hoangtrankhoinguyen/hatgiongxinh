@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Người dùng</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::open(['route' => ['users.update', $user->id],'method' => 'PUT']) }}
                                <div class="form-group">
                                    <label>Tên</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nhập tên" value="{{$user->name}}">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input tpye="text" name="email" class="form-control" placeholder="Nhập Email" value="{{$user->email}}">
                                </div>
                                <div class="form-group">
                                    <label>Mật khẩu</label>
                                    <input type="password" name="password" class="form-control" placeholder="Mật Khẩu" value="{{$user->password}}">
                                </div>
                                <button type="submit" class="btn btn-default">Gởi</button>
                                <button type="reset" class="btn btn-default">Làm mới</button>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()