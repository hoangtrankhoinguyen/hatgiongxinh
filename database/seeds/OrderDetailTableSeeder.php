<?php

use Illuminate\Database\Seeder;
use App\Order;
use App\Product;
use App\OrderDetail;
use Faker\Factory as Faker;

class OrderDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $orders = Order::all()->pluck('id')->all();
        $products = Product::all()->pluck('id')->all();
        for ($i=0; $i < 200; $i++) {
        	$pro = Product::find(rand(1,100));
        	$quantity = rand(1,10);
            OrderDetail::create([
                'product_id' => $pro->id,
                'quantity' => $quantity,
                'price' => $pro->price,
                'total' => $pro->price * $quantity,
                'unit' => $pro->unit,
                'order_id' => $faker->randomElement($orders),
            ]);
        }

    }
}
