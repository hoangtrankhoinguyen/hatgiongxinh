<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
                'name' => "Tran Van Diep",
                'password' => bcrypt("123456"),
                'email' => "anhdiep@gmail.com",
                'role' => '1'
            ]);

        $faker = Faker::create();
        foreach(range(1, 50) as $index)
        {
            \App\User::create([
                'name' => $faker->name(),
                'password' => bcrypt("secret"),
                'email' => $faker->email()
            ]);
        }
    }
}
