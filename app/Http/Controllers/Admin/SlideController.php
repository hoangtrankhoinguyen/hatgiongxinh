<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;
use Image;
use File;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::all();
        return view('admin.slide.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->image;

        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $path = public_path('uploads/slide/' . $filename);

        Image::make($image->getRealPath())->resize(1200, 462)->save($path);

        $slide = new Slide;

        $data = $request->all();

        $data['image'] = $filename;
        
        $slide = Slide::create($data);

        return redirect()->route('slides.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::findorFail($id);
        return view('admin.slide.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slide = Slide::findorFail($id);

        if(isset($request->image)) {
            $image_path = public_path()."/uploads/slide/".$slide->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $image = $request->image;

            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('uploads/slide/' . $filename);

            Image::make($image->getRealPath())->resize(1200, 462)->save($path);

            $data = $request->all();

            $data['image'] = $filename;
            
            $slide->update($data);

            return redirect()->route('slides.index');
        }

        $slides->update($request->all());

        return redirect()->route('slides.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::findorFail($id);

        if ($slide->image) {
            $image_path = public_path()."/uploads/slide/".$slide->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        $slide->delete();

        return redirect()->route('slides.index');
    }
}
