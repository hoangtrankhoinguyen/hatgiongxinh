@extends('layout.client.masterLayout')

@section('content')

<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 header-top-right">
                <div class="homeslider">
                    <ul id="contenhomeslider">
                        @foreach($slides as $slide)
                        <li><img alt="Funky roots" src="{{asset('uploads/slide/'.$slide->image)}}" title="Funky roots" /></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Home slideder-->

<div class="content-page">
    <div class="container">
        @foreach($categories as $category)
        <div class="category-featured Vegetables">
            <nav class="navbar nav-menu show-brand">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-brand">
                        <a href="#"> <img alt="fashion" src="{{asset('assets/client/images/flower.png')}}" />{{$category->name}}</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">

                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->

            </nav>
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-right">
                            <ul class="product-list">
                                @foreach(\App\Product::where('category_id', $category->id)->get() as $product)
                                <li class="col-sm-4 product-item">
                                    <div class="left-block">
                                        <a><img class="img-responsive" alt="product" src="{{asset('uploads/'.$product->image)}}" /></a>
                                        <div class="add-to-cart">
                                            <a class="add-item-to-cart" title="Đặt hàng" href="{{route('addSP',$product->id)}}" data-name="{{$product->name}}" data-price="{{$product->price}}">Đặt hàng</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="{{route('san-pham', $product->slug)}}">{{$product->name}}</a></h5>

                                        <div class="content_price">
                                            <span class="price product-price"><span style="color: black">Giá:</span> {{number_format($product->price)}} đ</span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection