<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Contact;
use Toast;
use App\Slide;

class HomepageController extends Controller
{	
    public function index(){
    	$categories = Category::where('type', 1)->where('isIndex', 1)->get();
        $slides = Slide::all();
    	return view('client.homepage', compact('categories','slides'));
    }

    public function getContact(){
    	return view('client.contact');
    }

    public function postContact(Request $request){
    	$contact = Contact::create($request->all());
    	Toast::success('message', 'title');
        return redirect()->route('lien-he');
    }
}
