@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sua Danh muc</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::open(['route' => ['categories.update', $category->id],'method' => 'PUT']) }}
                                <div class="form-group">
                                    <label>Tên</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nhập tên" value="{{$category->name}}">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" rows="3">{{$category->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        @if($category->status == "1")
                                        <option value="1" selected="">Hien Thi</option>
                                        <option value="0">An</option>
                                        @else
                                        <option value="1">Hien Thi</option>
                                        <option value="0" selected="">An</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Loai</label>
                                    <select class="form-control" name="type">
                                        @if($category->type == 1)
                                        <option value="1" selected="">San pham</option>
                                        <option value="2">Tin Tuc</option>
                                        <option value="3">Page</option>
                                        @elseif($category->type == 2)
                                        <option value="1">San pham</option>
                                        <option value="2" selected="">Tin Tuc</option>
                                        <option value="3">Page</option>
                                        @else
                                        <option value="1">San pham</option>
                                        <option value="2">Tin Tuc</option>
                                        <option value="3" selected="">Page</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Hien thi trang chu</label>
                                    <select class="form-control" name="isIndex">
                                        @if($category->isIndex == 0)
                                        <option selected="" value="0">Khong</option>
                                        <option value="1">Co</option>
                                        @else
                                        <option value="0">Khong</option>
                                        <option selected="" value="1">Co</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Danh Muc</label>
                                    <select class="form-control" name="parent_id">
                                        <option value="0">Cha</option>
                                        @if(isset($categories))
                                        @foreach($categories as $cate)
                                            <option value="{{$cate->id}}" 
                                                @if($category->parent_id == $cate->id) selected="" @endif
                                            >{{$cate->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Gởi</button>
                                <button type="reset" class="btn btn-default">Làm mới</button>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()