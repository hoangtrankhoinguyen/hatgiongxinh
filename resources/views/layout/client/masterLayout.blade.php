    <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="{{asset('assets/client/images/favicon.ico')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/reset.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/bootstrap/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/jquery.bxslider/jquery.bxslider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/owl.carousel/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/jquery-ui/jquery-ui.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/lib/fancyBox/source/jquery.fancybox.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/green.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/responsive.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/client/css/style_frm_dkdn.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/client/css/switcher.css')}}">
    <title>Eco Shop</title>
</head>

<body class="green">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1994785710735796&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- HEADER -->
    <div id="header" class="header">

        <!-- MAIN HEADER -->
        <div class="container main-header">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 logo">
                    <a href="{{route('trangChu')}}"><img alt="Eco Shop" src="{{asset('assets/client/images/logo.png')}}" /></a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 group-button-header"></div>
                <div class="col-xs-4 col-sm-12 col-md-5 col-lg-4 header-search-box">
                    {{ Form::open(['route' => 'search','method' => 'POST','class'=>'form-inline']) }}
                        <div class="form-group input-serach">
                            <input type="text" name="search" placeholder="Search ...">
                        </div>
                        <button type="submit" class="pull-right btn-search"><i class="fa fa-search"></i></button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        @include('toast::messages-jquery')      
        <!-- END MANIN HEADER -->
        <div id="nav-top-menu" class="nav-top-menu">
            <div class="container">
                <div class="row">

                    <div id="main-menu" class="col-sm-12 main-menu">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                    <a class="navbar-brand" href="#">MENU</a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="{{route('trangChu')}}"><span class="fa fa-home"></span> Home</a></li>
                                        @foreach(App\Category::with('Childs')->where('parent_id', 0)->get() as $item)
                                        @if($item->type == 1)
                                            @if($item->Childs->count()>0)
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$item->name}}</a>
                                                    <ul class="dropdown-menu container-fluid">
                                                        <li class="block-container">
                                                            <ul class="block">
                                                            @foreach($item->Childs as $subMenu)
                                                                <li class="link_container">
                                                                    <a href="{{route('danh-muc', $item->slug)}}">{{$subMenu->name}}
                                                                    </a></li>
                                                            @endforeach
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="{{route('danh-muc', $item->slug)}}">{{$item->name}}</a></li>
                                            @endif
                                        @endif
                                        @if($item->type == 2)
                                            @if($item->Childs->count()>0)
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$item->name}}</a>
                                                    <ul class="dropdown-menu container-fluid">
                                                        <li class="block-container">
                                                            <ul class="block">
                                                            @foreach($item->Childs as $subMenu)
                                                                <li class="link_container">
                                                                    <a href="{{route('tin-tuc', $item->slug)}}">{{$subMenu->name}}
                                                                    </a></li>
                                                            @endforeach
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="{{route('tin-tuc', $item->slug)}}">{{$item->name}}</a></li>
                                            @endif
                                        @endif
                                        @if($item->type == 3)
                                            @if($item->Childs->count()>0)
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$item->name}}</a>
                                                    <ul class="dropdown-menu container-fluid">
                                                        <li class="block-container">
                                                            <ul class="block">
                                                            @foreach($item->Childs as $subMenu)
                                                                <li class="link_container">
                                                                    <a href="{{route('danh-muc', $item->slug)}}">{{$subMenu->name}}
                                                                    </a></li>
                                                            @endforeach
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="{{route('danh-muc', $item->slug)}}">{{$item->name}}</a></li>
                                            @endif
                                        @endif
                                        @endforeach
                                        <li><a href="{{route('lien-he')}}"><span class="fa fa-phone"></span> Lien he</a></li>
                                    </ul>
                                </div>
                                <!--/.nav-collapse -->
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- userinfo on top-->
                <div id="form-search-opntop">
                </div>
                <!-- userinfo on top-->
                <div id="user-info-opntop">
                </div>
                <!-- CART ICON ON MMENU -->
            </div>
        </div>
    </div>
    <!-- end header -->

    @yield('content')

    <div class="demo_changer" id="demo_changer">
    <div class="demo-icon fa fa-shopping-cart" id="muaHang">
        <a href="{{route('muaHang')}}"><span class="notify notify-left" id="count-cart">{{Cart::count()}}</span></a>
    </div>
    <div class="form_holder">
        <div class="cart-block">
            <div class="cart-block-content">
                <div class="cart-block-list">
                    <div class="shop_cart">
                        <table class="shop_table">
                            <tbody id="show-cart">
                                <!--/////////////////////////////////-->
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!---->
    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
                <div class="col-md-3">
                    <div id="address-box">
                        <a href="#"><img src="{{asset('assets/client/images/logo.png')}}" alt="logo"></a>

                        <div id="address-list">
                            <div class="tit-name">Địa chỉ: </div>
                            <div class="tit-contain">160 Đoàn Phú Tứ - Liên Chiểu - Đà Nẵng</div>
                            <div class="tit-name">Hotline:</div>
                            <div class="tit-contain">0975.310.717 - 0905.951.448</div>
                            <div class="tit-name">Email:</div>
                            <div class="tit-contain">trandiep56@gmail.com</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="introduce-title">CUSTOMER CARE</div>
                            <ul id="introduce-support" class="introduce-list">
                                <li> Mon - Fri: 7am to 8pm </li>
                                <li>Sat &amp; Sun: 7am to 6pm</li>
                                <li><a href="policy-faq.html">How to Buy</a></li>
                                <li><a href="policy-faq.html">How to Payment</a></li>
                                <li><a href="policy-faq.html">FAQ</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div class="introduce-title">Policy</div>
                            <ul id="introduce-Account" class="introduce-list">
                                <li><a href="policy-faq.html">Security Policy</a></li>
                                <li><a href="policy-faq.html">Payment Policy</a></li>
                                <li><a href="policy-faq.html">Return &amp; Change Policy</a></li>
                                <li><a href="policy-faq.html">Terms &amp; Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div id="contact-box">
                        <div class="introduce-title">Follow Us</div>
                        <!--<div class="social-link">-->
                        <!--<a href="#"><i class="fa fa-facebook"></i></a>-->
                        <!--<a href="#"><i class="fa fa-pinterest-p"></i></a>-->
                        <!--<a href="#"><i class="fa fa-google-plus"></i></a>-->
                        <!--<a href="#"><i class="fa fa-youtube"></i></a>-->
                        <!--</div>-->
                        <div class="social-link">
                            <ul class="social-icons">
                                <li>
                                    <a class="rss" data-original-title="rss" href="#"></a>
                                </li>
                                <li>
                                    <a class="facebook" data-original-title="facebook" href="#"></a>
                                </li>
                                <li>
                                    <a class="twitter" data-original-title="twitter" href="#"></a>
                                </li>
                                <li>
                                    <a class="googleplus" data-original-title="googleplus" href="#"></a>
                                </li>
                                <li>
                                    <a class="linkedin" data-original-title="linkedin" href="#"></a>
                                </li>
                                <li>
                                    <a class="youtube" data-original-title="youtube" href="#"></a>
                                </li>
                                <li>
                                    <a class="vimeo" data-original-title="vimeo" href="#"></a>
                                </li>
                                <li>
                                    <a class="skype" data-original-title="skype" href="#"></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /#introduce-box -->


            <div id="footer-menu-box">
                <p class="text-center">Copyrights © 2016 Eco Shop. All Rights Reserved.</p>
            </div>
            <!-- /#footer-menu-box -->
        </div>
    </footer>

    <a href="# " class="scroll_top " title="Scroll to Top " style="display: inline; ">Scroll</a>
    <!-- Script-->

    <script type="text/javascript " src="{{asset('assets/client/lib/jquery/jquery-1.11.2.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/jquery-migrate.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/fancyBox/source/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/select2/js/select2.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/jquery.bxslider/jquery.bxslider.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/lib/owl.carousel/owl.carousel.min.js')}}"></script>

    <script type="text/javascript " src="{{asset('assets/client/js/jquery.actual.min.js')}}"></script>
    <script type="text/javascript " src="{{asset('assets/client/js/theme-script.js')}}"></script>

    <script type="text/javascript " src="{{asset('assets/client/js/layout.js')}}"></script>

    <script>
        $(document).ready(function(){
            $("#muaHang").click(function(){
                window.location.href = "{{route('muaHang')}}";
            });
        });
    </script>
    @yield('js')
</body>

</html>