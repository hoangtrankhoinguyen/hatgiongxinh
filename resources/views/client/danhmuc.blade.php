@extends('layout.client.masterLayout')
@section('content')
<div class="columns-container">
    <div class="container" id="columns">
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block"><span class="fa fa-pagelines"></span> SẢN PHẨM</p>

                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                	@foreach($sidebar as $item)
                                    <li>
                                        <span></span><a href="{{route('san-pham', $item->slug)}}">{{$item->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- block best sellers -->
                <!--<div class="block left-module">-->
                <!--<p class="title_block">VIDEO</p>-->

                <!--<div class="block_content">-->
                <!--<iframe width="255" height="300" src="https://www.youtube.com/embed/oFuC0URYKqU" frameborder="0"-->
                <!--allowfullscreen=""></iframe>-->
                <!--</div>-->
                <!--</div>-->
                <!-- ./block best sellers  -->

                <!-- left silide -->
                <!--????????????????-->
                <!--./left silde-->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title">{{$category->name}}</span>
                </h2>
                <!-- ../page heading-->
                <!-- Product -->
                <div id="product">
                    <!-- box product -->
                    <div class="category-featured Vegetables">
                        <div class="product-featured clearfix">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box-right">
                                        <ul class="product-list">
                                        	@foreach($products as $product)
                                            <li class="col-sm-3 product-item">
                                                <div class="left-block">
                                                    <a><img class="img-responsive" alt="product" src="{{asset('uploads/'.$product->image)}}" /></a>

                                                    <div class="add-to-cart">
                                                        <a class="add-item-to-cart" title="Add to Cart" href="{{route('addSP',$product->slug)}}" data-name="{{$product->name}} - {{$product->unit}}" data-price="{{$product->price}}" data-product-code="{{$product->id}}}">Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="{{route('san-pham', $product->slug)}}">{{$product->name}} - {{$product->unit}}</a></h5>

                                                    <div class="content_price">
                                                        <span class="price product-price"></span>
                                    					<span style="color: orangered">Best Price:</span> {{$product->price or "LIEN HE"}}đ</span>
                                                        <!--<br><span style="color: black">Price:</span>-->
                                                        <!--<span class="price old-price">55,000đ</span>-->
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./box product -->
                </div>
                <!-- Product -->

                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            {{ $products->links() }}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
@endsection