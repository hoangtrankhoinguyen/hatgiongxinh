<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cart;
use Toast;
use App\Order;
use App\OrderDetail;

class ShoppingController extends Controller
{
    public function index(){
    	$cartCollection = Cart::content();
    	return view('client.shopping',compact('cartCollection'));
    }

    public function add($id){
    	$product = Product::where('id', $id)->first();
    	Cart::add($id, $product->name, 1, $product->price, ['image' => $product->image]);
        Toast::success('message', 'title');
    	return redirect()->back();
    }

    public function delete($id){
    	Cart::remove($id);
        Toast::success('message', 'title');
    	return redirect()->back();
    }

    public function update(Request $request, $id){
    	Cart::update($id, $request->quantity);
        Toast::success('message', 'title');
    	return redirect()->back();
    }

    public function checkout(){
        return view('client.checkout');
    }

    public function doCheckout(Request $request){
        $data = $request->all();
        $data['status'] = "1";
        $order = Order::create($data);

        $dataOrderDetailRaw = Cart::content();
        foreach ($dataOrderDetailRaw as $item) {
            $order_detail = new OrderDetail;
            $order_detail->product_id = $item->id;
            $order_detail->order_id = $order->id;
            $order_detail->quantity = $item->qty;
            $order_detail->price = $item->price;
            $order_detail->total = $item->total;
            $order_detail->unit = Product::findorFail($item->id)->unit;
            $order_detail->save();
        }

        Cart::destroy();
        Toast::success('Bạn đã đặt hàng thành công', 'Thông báo');
        return redirect()->route('trangChu');
    }
}
