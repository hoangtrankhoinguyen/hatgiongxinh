@extends('layout.admin.masterLayout')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Product</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::open(['route' => 'products.store','method' => 'POST','files' => true]) }}
                                <div class="form-group">
                                    <label>Tên</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nhập tên">
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input tpye="text" name="price" class="form-control" placeholder="Nhập Gia">
                                </div>
                                <div class="form-group">
                                    <label>Don vi (Unit)</label>
                                    <input tpye="text" name="unit" class="form-control" placeholder="Nhập Don Vi">
                                </div>
                                <div class="form-group">
                                    <label>Anh</label>
                                    <input type="file" name="image">
                                </div>
                                <div class="form-group">
                                    <label>Mo ta ngan</label>
                                    <textarea class="form-control" name="short_description" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Mo ta</label>
                                    <textarea class="form-control" id="product_long_description" name="long_description" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Danh Muc</label>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Gởi</button>
                                <button type="reset" class="btn btn-default">Làm mới</button>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()