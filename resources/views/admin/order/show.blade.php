@extends('layout.admin.masterLayout')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hien Thi</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <h2>Invoice for purchase #{{$order->id}}</h2>
                {{ Form::open(['route' => ['orders.update', $order->id],'method' => 'PUT']) }}
                    <div class="form-group">
                    @if($order->status == 1)
                        <label class="radio-inline">
                        <input type="radio" name="status" value="1" checked="">Chua xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="2">Dang xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="3">Da xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="4">Da huy
                        </label>
                    @elseif($order->status == 2)
                        <label class="radio-inline">
                        <input type="radio" name="status" value="1">Chua xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="2" checked="">Dang xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="3">Da xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="4">Da huy
                        </label>
                    @elseif($order->status == 3)
                        <label class="radio-inline">
                        <input type="radio" name="status" value="1">Chua xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="2" >Dang xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="3" checked="">Da xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="4">Da huy
                        </label>
                    @else
                        <label class="radio-inline">
                        <input type="radio" name="status" value="1">Chua xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="2" >Dang xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="3">Da xu ly
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="4" checked="">Da huy
                        </label>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Save changes</button>
                {{ Form::close() }}
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Billing Details</div>
                        <div class="panel-body">
                            <strong>{{$order->client_name}}</strong><br>
                            <strong>{{$order->client_phone}}</strong><br>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 pull-right">
                    <div class="panel panel-default height">
                        <div class="panel-heading">Address</div>
                        <div class="panel-body">
                            <strong>{{$order->client_address}}</strong><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Item Name</strong></td>
                                    <td class="text-center"><strong>Item Price</strong></td>
                                    <td class="text-center"><strong>Item Quantity</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order_detail as $detail)
                                <tr>
                                    <td>{{$detail->Product->name}}</td>
                                    <td class="text-center">{{number_format($detail->price)}}</td>
                                    <td class="text-center">{{$detail->quantity}}</td>
                                    <td class="text-right">{{number_format($detail->total)}}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Total</strong></td>
                                    <td class="emptyrow text-right">{{number_format($order_detail->sum('total'))}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection()