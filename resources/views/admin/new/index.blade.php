@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href ="{{route('news.create')}}" class="btn btn-primary">Thêm mới</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Status</th>
                                <th>Thuoc danh muc</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($news as $new)
                                <tr class="odd gradeX">
                                    <td>{{$new->title}}</td>
                                    <td>
                                        @if($new->status == 1)
                                            Hien    
                                        @else
                                            An
                                        @endif
                                    </td>
                                    <td>
                                        {{$new->Category->name}}
                                    </td>
                                    <td>
                                        <a href ="{{route('news.edit', $new->id)}}" class="btn btn-info">Sửa</a>
                                        <a href ="{{route('news.show', $new->id)}}" class="btn btn-primary">Hien Thi</a>
                                        <a href ="{{route('news.destroy', $new->id)}}" class="btn btn-danger" onclick="return confirm('Xóa bai viet {{$new->title}}')">Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

@endsection

@section('css')
<!-- DataTables CSS -->
<link href="{{asset('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{asset('assets/admin/vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">
@endsection