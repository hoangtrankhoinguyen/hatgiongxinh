<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $limit = 20;
        for ($i=0; $i < $limit; $i++) 
        { 
            $name = $faker->sentence($nbWords = 2, $variableNbWords = true);
            \App\Category::create([
                'name' => $name,
                'slug' => str_slug($name),
                'description' => $faker->text($maxNbChars = 150),
                'status' => rand(0, 1),
                'type' => rand(1,3),
                'parent_id' => rand(0, 5)
            ]);
        }
    }
}
