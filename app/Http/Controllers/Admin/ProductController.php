<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Image;
use App\Category;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.product.index', compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('type',1)->get();
        return view('admin.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->image;

        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $path = public_path('uploads/' . $filename);

        Image::make($image->getRealPath())->resize(200, 200)->save($path);

        $product = new Product;

        $data = $request->all();

        $data['image'] = $filename;
        
        $product = Product::create($data);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findorFail($id);

        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('type',1)->get();
        $product = Product::findorFail($id);
        return view('admin.product.edit', compact('categories','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findorFail($id);

        if(isset($request->image)) {
            $image_path = public_path()."/uploads/".$product->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $image = $request->image;

            $filename  = time() . '.' . $image->getClientOriginalExtension();

            $path = public_path('uploads/' . $filename);

            Image::make($image->getRealPath())->resize(200, 200)->save($path);

            $data = $request->all();

            $data['image'] = $filename;
            
            $product->update($data);
            return redirect()->route('products.index');
        }

        $product->update($request->all());

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findorFail($id);

        if ($product->image) {
            $image_path = public_path()."/uploads/".$product->image;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        $product->delete();

        return redirect()->route('products.index');
    }
}
