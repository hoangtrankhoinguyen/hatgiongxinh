<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\News;

class CategoryController extends Controller
{
    public function getCategory($slug){
    	$category = Category::where('slug', $slug)->first();
    	$products = Product::where('category_id', $category->id)->paginate(2);
    	$sidebar = Product::where('category_id', $category->id)->orderBy('created_at', 'ASC')->get()->take(10);
    	return view('client.danhmuc', compact('products','category','sidebar'));
    }

    public function getSP($slug){
    	$product = Product::where('slug', $slug)->first();
    	$sidebar = Product::where('category_id', $product->category_id)->orderBy('created_at', 'ASC')->get()->take(10);
    	$productRelevant = Product::where('category_id', $product->category_id)->orderBy('created_at', 'DESC')->get()->take(10);
    	return view('client.sanpham', compact('product','sidebar','productRelevant'));    
    }

    public function getTinTuc($id){
        $category = Category::findorfail($id);
        $tintucs = News::where('category_id', $id)->paginate(10);
        $sidebar = Category::where('type', 2)->orderBy('created_at', 'ASC')->get()->take(10);
        return view('client.tintuc', compact('category', 'tintucs','sidebar'));
    }

    public function getChiTiet($id){
        $new = News::findorFail($id);
        $sidebar = Category::where('type', 2)->orderBy('created_at', 'ASC')->get()->take(10);

        return view('client.chitiet', compact('new', 'sidebar'));
    }
}
