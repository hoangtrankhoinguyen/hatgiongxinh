@extends('layout.client.masterLayout')
@section('content')
<div class="columns-container">
    <div class="container" id="columns">
        <!-- row -->
        <div class="row">
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title">Co {{count($products)}} ket qua voi tu khoa {{$keyword}}</span>
                </h2>
                <!-- ../page heading-->
                <!-- Product -->
                <div id="product">
                    <!-- box product -->
                    <div class="category-featured Vegetables">
                        <div class="product-featured clearfix">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box-right">
                                        <!--&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;&#45;-->
                                        <ul class="product-list">
                                            @foreach($products as $product)
                                            <li class="col-sm-3 product-item">
                                                <div class="left-block">
                                                    <a><img class="img-responsive" alt="product" src="{{asset('uploads/'.$product->image)}}" /></a>

                                                    <div class="add-to-cart">
                                                        <a class="add-item-to-cart" title="Add to Carr" href="{{route('addSP',$product->id)}}" data-name="{{$product->name}} - {{$product->unit}}" data-price="{{$product->price}}" data-product-code="{{$product->id}}}">Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="right-block">
                                                    <h5 class="product-name"><a href="{{route('san-pham', $product->id)}}">{{$product->name}} - {{$product->unit}}</a></h5>

                                                    <div class="content_price">
                                                        <span class="price product-price"></span>
                                                        <span style="color: orangered">Best Price:</span> {{$product->price or "LIEN HE"}}đ</span>
                                                        <!--<br><span style="color: black">Price:</span>-->
                                                        <!--<span class="price old-price">55,000đ</span>-->
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./box product -->
                </div>
                <!-- Product -->

                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            {{ $products->links() }}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
@endsection