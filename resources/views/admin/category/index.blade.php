@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Categories</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href ="{{route('categories.create')}}" class="btn btn-primary">Thêm mới</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Status</th>
                                <th>Thuoc danh muc</th>
                                <th>Loai danh muc</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                                <tr class="odd gradeX">
                                    <td>{{$category->name}}</td>
                                    <td>
                                        @if($category->status == 1)
                                            Hien    
                                        @else
                                            An
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        $cate = \App\Category::find($category->parent_id);
                                        if ($cate == null) {
                                            echo "Day la danh muc cha";
                                        } else {
                                            echo $cate->name;
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        @if($category->type == 1)
                                            San pham
                                        @elseif($category->type == 2)
                                            Tin tuc
                                        @else
                                            Static
                                        @endif
                                    </td>
                                    <td>
                                        <a href ="{{route('categories.edit', $category->id)}}" class="btn btn-info">Sửa</a>
                                        <a href ="{{route('categories.show', $category->id)}}" class="btn btn-primary">Hien Thi</a>
                                        <a href ="{{route('categories.destroy', $category->id)}}" class="btn btn-danger" onclick="return confirm('Xóa danh muc {{$category->name}}')">Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

@endsection

@section('css')
<!-- DataTables CSS -->
<link href="{{asset('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{asset('assets/admin/vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">
@endsection