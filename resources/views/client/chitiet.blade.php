@extends('layout.client.masterLayout')
@section('content')
<div class="columns-container page-contact-us">
	<div class="container" id="columns">
		<!-- row -->
		<div class="row">
			<!-- Left colunm -->
			<div class="column col-xs-12 col-sm-3" id="left_column">
				<!-- block category -->
				<div class="block left-module">
					<p class="title_block"><span class="fa fa-list"></span> CATEGORIES</p>

					<div class="block_content">
						<!-- layered -->
						<div class="layered layered-category">
							<div class="layered-content">
								<ul class="tree-menu">
									@foreach($sidebar as $item)
									<li>
										<span></span><a href="{{route('chi-tiet', $item->id)}}">{{$item->name}}</a>
									</li>
									@endforeach
								</ul>
							</div>
						</div>
						<!-- ./layered -->
					</div>
				</div>
				<!-- ./block category  -->
				<!-- block video -->
				<div class="block left-module">
					<p class="title_block"><span class="fa fa-video-camera"></span> VIDEOS</p>

					<div class="block_content">
						<iframe width="255" height="300" src="https://www.youtube.com/embed/oFuC0URYKqU" frameborder="0" allowfullscreen=""></iframe>
					</div>
				</div>
				<!-- ./block video  -->

				<!-- block image -->

				<!-- ./block image  -->
			</div>

			<!-- Center colunm-->
			<div class="center_column col-xs-12 col-sm-9" id="center_column">
				<!-- page heading-->
				<div class="page-heading">
					<h2>
						<span class="page-heading-title"><span class="fa fa-paperclip"></span> {{$new->title}} </span>
					</h2>
				</div>

				<div class="entry-meta-data" style="float: right; padding-top: 10px; padding-bottom: 10px">
					<span class="author">
						<i class="fa fa-user"></i>
						<span class="date"><i class="fa fa-calendar"></i> {{$new->created_at}} </span>
					</div>

					<div style="padding-top: 50px">
						{!!$new->content!!}
					</div>


				</div>
				<!-- ./ Center colunm -->
			</div>
			<!-- ./row-->
		</div>
	</div>
	@endsection