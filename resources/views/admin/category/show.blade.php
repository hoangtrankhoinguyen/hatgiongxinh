@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hien thi Danh muc</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tên</label>
                                    <p class="form-control-static">{{$category->name}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <p class="form-control-static">{{$category->description}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <p class="form-control-static">
                                        @if($category->status == "1")
                                            Hien thi
                                        @else
                                            An
                                        @endif
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label>Loai</label>
                                    <p class="form-control-static">
                                        @if($category->type == 1)
                                        San Pham
                                        @elseif($category->type == 2)
                                        Tic tuc
                                        @else
                                        Static
                                        @endif
                                    </p>
                                </div>
                                <div class="form-group">
                                    <label>Danh Muc</label>
                                    <p class="form-control-static">
                                        <?php
                                        $cate = \App\Category::find($category->parent_id);
                                        if ($cate == null) {
                                            echo "Day la danh muc cha";
                                        } else {
                                            echo $cate->name;
                                        }
                                        ?>
                                    </p>
                                </div>
                                <a href="{{route('categories.edit', $category->id)}}" class="btn btn-default">Edit</a>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()