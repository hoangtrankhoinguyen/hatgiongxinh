<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
	public function search(Request $request){
		$keyword = $request->search;
		$products = Product::where('name', 'like', '%'.$request->search.'%')
		->orderBy('name')
		->paginate(20);
		return view('client.ketqua', compact('products', 'sidebar', 'keyword'));
	}
}
