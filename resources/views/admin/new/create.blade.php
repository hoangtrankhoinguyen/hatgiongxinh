@extends('layout.admin.masterLayout')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Bai viet</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {{ Form::open(['route' => 'news.store','method' => 'POST','files' => true]) }}
                                <div class="form-group">
                                    <label>Tên</label>
                                    <input type="text" name="title" class="form-control" placeholder="Nhập tên">
                                </div>
                                <div class="form-group">
                                    <label>Mo ta</label>
                                    <textarea class="form-control" id="product_long_description" name="content" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Danh Muc</label>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1">Hien Thi</option>
                                        <option value="0">An</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Gởi</button>
                                <button type="reset" class="btn btn-default">Làm mới</button>
                            {{ Form::close() }}
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()