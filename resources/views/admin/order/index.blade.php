@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Categories</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ten khach hang</th>
                                <th>So dien thoai</th>
                                <th>Dia Chi</th>
                                <th>Status</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr class="odd gradeX">
                                    <td>{{$order->id}}</td>
                                    <td>
                                        {{$order->client_name}}
                                    </td>
                                    <td>
                                        {{$order->client_phone}}
                                    </td>
                                    <td>
                                        {{$order->client_address}}
                                    </td>
                                    <td>
                                        @if($order->status == 1)
                                            Chua xu ly  
                                        @elseif($order->status == 2)
                                            Dang xu ly
                                        @elseif($order->status == 3)
                                            Da xu ly
                                        @else
                                            Da huy
                                        @endif
                                    </td>
                                    <td>
                                        <a href ="{{route('orders.show', $order->id)}}" class="btn btn-primary">Hien Thi</a>
                                        <a href ="{{route('orders.destroy', $order->id)}}" class="btn btn-danger" onclick="return confirm('Xóa don hang {{$order->name}}')">Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->
@endsection

@section('css')
<!-- DataTables CSS -->
<link href="{{asset('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{asset('assets/admin/vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">
@endsection