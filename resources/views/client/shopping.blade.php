@extends('layout.client.masterLayout')

@section('content')
<!-- page wapper-->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title">Shopping Cart</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">

            <div class="heading-counter warning"><span class="fa fa-shopping-cart"></span> Your shopping cart contains:
                <span id="count-cart" style="font-weight: bold">{{Cart::count()}}</span> <span> Products</span>
            </div>
            <div class="order-detail-content">
                <table id="cart" class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style="width:50%">Product</th>
                            <th style="width:10%">Price</th>
                            <th style="width:8%">Quantity</th>
                            <th style="width:22%" class="text-center">Subtotal</th>
                            <th style="width:10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(Cart::content() as $row)
                        {{ Form::open(['route' => ['updateSP', $row->rowId],'method' => 'PUT']) }}
                        <tr>
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2 hidden-xs"><img src="{{asset('uploads/'.$row->options->image)}}" alt="..." class="img-responsive"/></div>
                                    <div class="col-sm-10">
                                        <h4 class="nomargin">{{$row->name}}</h4>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">{{$row->price}}</td>
                            <td data-th="Quantity">
                                <input type="number" name="quantity" class="form-control text-center" value="{{$row->qty}}">
                            </td>
                            <td data-th="Subtotal" class="text-center">{{number_format($row->total)}}</td>
                            <td class="actions" data-th="">

                                <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
                                <a href="{{route('xoaSP', $row->rowId)}}" class="btn btn-danger btn-sm" onclick="return confirm('Xóa SP {{$row->name}}')"><i class="fa fa-trash-o"></i></a>                                
                            </td>
                        </tr>
                        {{ Form::close() }}
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr class="visible-xs">
                            <td class="text-center"><strong>Total {{Cart::subtotal(0)}}</strong></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                            <td colspan="2" class="hidden-xs"></td>
                            <td class="hidden-xs text-center"><strong>Total {{Cart::subtotal(0)}}</strong></td>
                            <td><a href="{{route('checkout')}}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->
@endsection

