<?php

use Illuminate\Database\Seeder;
use App\Category;
use Faker\Factory as Faker;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $categories = Category::where('type', 2)->pluck('id')->all();
        foreach (range(1, 100) as $index) {
            $title = $faker->sentence($nbWords = 2, $variableNbWords = true);
            \App\News::create([
				'title' => $title,
                'slug' => str_slug($title),
				'content' => $faker->realText($maxNbChars = 1000, $indexSize = 4),
				'status' => rand(0,1),            
				'category_id' => $faker->randomElement($categories),      
                'created_at' => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now')
            ]);
        }
    }
}
