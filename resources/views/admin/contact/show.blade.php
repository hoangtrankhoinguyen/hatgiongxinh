@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Hien thi lien he</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tên</label>
                                    <p class="form-control-static">{{$contact->name}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <p class="form-control-static">{{$contact->email}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <p class="form-control-static">{{$contact->phone}}</p>
                                </div>
                                <div class="form-group">
                                    <label>Noi Dung</label>
                                    <p class="form-control-static">{{$contact->content}}</p>
                                </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()