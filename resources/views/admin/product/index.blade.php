@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Product</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href ="{{route('products.create')}}" class="btn btn-primary">Thêm mới</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Price</th>
                                <th>Danh Muc</th>
                                <th>Hinh Anh</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr class="odd gradeX">
                                    <td>{{$product->name}}</td>
                                    <td>{{number_format($product->price), 2}}</td>
                                    <td>{{$product->Category->name}}</td>
                                    <td><img src="{{asset('uploads/'.$product->image)}}" width="50px", height="50px" /></td>
                                        <td><a href ="{{route('products.edit', $product->id)}}" class="btn btn-info">Sửa</a>
                                        <a href ="{{route('products.show', $product->id)}}" class="btn btn-primary">Hien Thi</a>
                                        <a href ="{{route('products.destroy', $product->id)}}" class="btn btn-danger" onclick="return confirm('Xóa hinh anh {{$product->name}}')">Xóa</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
<!-- /#page-wrapper -->

@endsection

@section('css')
<!-- DataTables CSS -->
<link href="{{asset('assets/admin/vendor/datatables-plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{asset('assets/admin/vendor/datatables-responsive/dataTables.responsive.css')}}" rel="stylesheet">
@endsection