@extends('layout.client.masterLayout')
@section('content')
<div class="columns-container">
    <div class="container" id="columns">
        <!-- row -->
        <div class="row">
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block"><span class="fa fa-pagelines"></span> PRODUCT</p>

                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                	@foreach($sidebar as $item)
                                    <li>
                                        <span></span><a href="{{route('san-pham', $item->slug)}}">{{$item->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- Product -->
                <div id="product">
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-sm-6">
                            <!-- product-imge-->
                            <div class="product-image">
                                <div class="product-full">
                                    <img id="product-zoom" src="{{asset('uploads/'.$product->image)}}" data-zoom-image="{{asset('uploads/'.$product->image)}}" />
                                </div>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-sm-6">
                            <h1 class="product-name">{{$product->name}}</h1>

                            <div class="product-price-group">
                                <span class="price product-price"><span
                                     style="color: black">Price:</span> {{number_format($product->price)}}</span>
                            </div>
                            <div class="product-desc">
                                {{$product->short_description}}
                            </div>

                            <div class="form-action">
                                <div class="button-group">
                                    <a class="add-item-to-cart btn-add-cart" title="Add to Cart" href="{{route('addSP', $product->id)}}" data-name="{{$product->name}} - {{$product->unit}}" data-price="{{$product->price}}" data-product-code="{{$product->id}}">Thêm vào giỏ</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- tab product -->
                    <div class="product-tab">
                        <ul class="nav-tab" style="margin-bottom: 0px;">
                            <li class="active">
                                <a aria-expanded="false" data-toggle="tab" href="#product-detail">Detail</a>
                            </li>

                        </ul>
                        <div class="tab-container">
                            <div id="product-detail" class="tab-panel active">
                                {!!$product->long_description!!}
                            </div>
                            <div id="comment" class="tab-panel">
                                <div class="fb-comments" data-href="https://www.facebook.com/hungdng.92" data-width="100%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="fb-comments" data-href="http://hatgiongxinh.local/" data-width="100%" data-numposts="5"></div>
                    <!-- ./tab product -->
                    <!-- box product -->
                    <div class="page-product-box">
                        <h2 class="page-heading">
                            <span class="page-heading-title"> HẠT GIỐNG LIÊN QUAN</span>
                        </h2>
                        <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav="true" data-margin="30" data-autoplayTimeout="1000" data-autoplayHoverPause="true">
                        	@foreach($productRelevant as $productLQ)
                            <li>
                                <div class="product-container">
                                    <div class="left-block">
                                        <a><img class="img-responsive" alt="product" src="{{asset('uploads/'.$productLQ->image)}}" /></a>

                                        <div class="add-to-cart">
                                            <a class="add-item-to-cart" title="Add to Cart" href="{{route('addSP', $productLQ->id)}}" data-name="{{$productLQ->name}} - {{$productLQ->unit}}" data-price="{{$productLQ->price}}" data-product-code="{{$productLQ->id}}">Add to Cart</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="#">{{$productLQ->name}} - {{$productLQ->unit}}</a></h5>

                                        <div class="content_price">
                                            <span class="price product-price"><span style="color: black">Price:</span> {{number_format($productLQ->price)}}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- ./box product -->
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
@endsection