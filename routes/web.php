<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', ['as' => 'LoginAdmin', 'uses' => 'Admin\UserController@getLogin']);
Route::post('admin/login', ['as' => 'PostLoginAdmin', 'uses' => 'Admin\UserController@postLogin']);
Route::get('admin', 'Controller@indexDasboard')->middleware('admin');
/* /========================ROUTE FOR ADMIN========================/ */
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['admin']], function () {
	// User
    Route::resource('users', 'UserController')->except(['destroy'
    ]);
    Route::get('users/destroy/{id}', ['as' => 'users.destroy', 'uses' => 'UserController@destroy']);
    // Category
    Route::resource('categories', 'CategoryController')->except(['destroy'
    ]);
    Route::get('categories/destroy/{id}', ['as' => 'categories.destroy', 'uses' => 'CategoryController@destroy']);
    // Product
    Route::resource('products', 'ProductController')->except(['destroy'
    ]);
    Route::get('products/destroy/{id}', ['as' => 'products.destroy', 'uses' => 'ProductController@destroy']);
    // New
    Route::resource('news', 'NewsController')->except(['destroy'
    ]);
    Route::get('news/destroy/{id}', ['as' => 'news.destroy', 'uses' => 'NewsController@destroy']);
    // Order
    Route::resource('orders', 'OrderController')->except(['destroy','create','store']);
    Route::get('orders/destroy/{id}', ['as' => 'orders.destroy', 'uses' => 'OrderController@destroy']);

    // Contact
    Route::resource('contacts', 'ContactController')->except(['destroy','edit','update', 'create', 'store']);
    Route::get('contacts/destroy/{id}', ['as' => 'contacts.destroy', 'uses' => 'ContactController@destroy']);

    // Slide
    Route::resource('slides', 'SlideController')->except(['destroy','show']);
    Route::get('slides/destroy/{id}', ['as' => 'slides.destroy', 'uses' => 'SlideController@destroy']);


});


Route::get('/', ['as' => 'trangChu','uses'=>'HomepageController@index']);
Route::get('mua-hang',['as' => 'muaHang', 'uses' => 'ShoppingController@index']);
Route::get('them-san-pham/{id}',['as'=>'addSP', 'uses'=>'ShoppingController@add']);
Route::get('xoa-san-pham/{id}',['as'=>'xoaSP', 'uses'=>'ShoppingController@delete']);
Route::put('update-san-pham/{id}',['as'=>'updateSP', 'uses'=>'ShoppingController@update']);
Route::get('thanh-toan', ['as' => 'checkout', 'uses'=>'ShoppingController@checkout']);
Route::post('thanh-toan', ['as'=>'doCheckout', 'uses' => 'ShoppingController@doCheckout']);

Route::get('danh-muc/{slug}', ['as' => 'danh-muc', 'uses'=>'CategoryController@getCategory']);
Route::get('san-pham/{slug}',['as'=>'san-pham','uses'=>'CategoryController@getSP']);

Route::get('tin-tuc/{id}', ['as' => 'tin-tuc', 'uses'=>'CategoryController@getTinTuc']);
Route::get('chi-tiet/{id}', ['as' => 'chi-tiet', 'uses'=>'CategoryController@getChiTiet']);

Route::get('lien-he', ['as' => 'lien-he', 'uses'=>'HomepageController@getContact']);
Route::post('lien-he', ['as' => 'post-lien-he', 'uses'=>'HomepageController@postContact']);

Route::post('search', ['as' => 'search', 'uses'=> 'SearchController@search']);
