<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            \App\Order::create([
                'status' => rand(1, 4),
                'client_name' => $faker->name,
                'client_phone' => $faker->tollFreePhoneNumber,
                'client_address' => $faker->address,
                'created_at' => $faker->dateTimeBetween($startDate = '-5 years', $endDate = 'now')
            ]);
        }

    }
}
