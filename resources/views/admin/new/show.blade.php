@extends('layout.admin.masterLayout')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Chi tiet</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Basic Form Elements
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tieu de</label>
                                <p class="form-control-static">{{$new->title}}</p>
                            </div>

                            <div class="form-group">
                                <label>Noi dung</label>
                                {!! $new->content !!}
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <p class="form-control-static">
                                    @if($new->status == "1")
                                        Hien thi
                                    @else
                                        An
                                    @endif
                                </p>
                            </div>
                            <div class="form-group">
                                <label>Danh Muc</label>
                                <p class="form-control-static">{{$new->Category->name}}</p>
                            </div>

                            <a href="{{route('news.edit', $new->id)}}" class="btn btn-default">Edit</a>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
@endsection()